# Recent Payments
## Table Component + Firebase + Vuex
Live: http://recentpayments.calirojas.net/

# Deployment
Project deployed to my Node.js hosting account. Generated with yarn build and uploaded.

## How long did you spend on the test? Would you do anything differently if you had more time?
14 hours spent. There are a lot of improvements to do, for example:
- Vuex communication with Firebase
- Improvements in the UI (functional and design)

The UI was made using only Bootstrap 4 classes and theme.

## In what ways would you adapt your component so that it could be used in many different scenarios where a data table is required?
Adding advanced options, for example: show and hide columns (customize), search by field (Name, Description, etc), multiple column sort, etc.

Currently the component has some props and events:

```html
<TableComponent
    v-if="isLoaded"
    :table-data="getPaymentsData"
    :table-headers="['ID', 'Name', 'Description', 'Date', 'Amount']"
    @delete-record="deleteRecord"
    @edit-record="editRecord"
    @delete-selection="deleteSelection"
/>
```
There are events events emitted when you try to delete or edit a record (@delete-record, @edit-record, @delete-selecion). Try adding and removing those events, one by one; to see how the UI changes the buttons and other options. The component UI is adaptable by itself.

You can specify the table header and data using props.

# What is your favorite CSS property? Why?
```css
display: flex;
```
Because... it's awesome!

## What is your favorite modern Javascript feature? Why?
New features for Arrays, such as map, filter, etc; because these methods are
easy to use and powerful.

## What is your favorite third-party Vue.js library? Why?
Quasar and Vuetify are great for mobile experiences.


# Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
