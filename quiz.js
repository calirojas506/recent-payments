// Assuming you have already done "npm install fernet"
let fernet = require('./node_modules/fernet')
let secret = new fernet.Secret('TluxwB3fV_GWuLkR1_BzGs1Zk90TYAuhNMZP_0q4WyM=')
// Oh no! The code is going over the edge! What are you going to do?
let message = 'gAAAAABcPbEekHQMUQe9lIZsced3k5FDOujKKmIn1BZpiXhw99Gma5PU2u9DzpoFgyIsA8MOOwku2o6OojWjbO-muFVFsReoRELJesDLNTTcOt0k7hg-AAdVHA76YkvgHyiYzhWwx_L1QffPwwvkznK92wwkBqizLaH9ChmzyuXQ9bsh40SccvrtSvzi7SX7TYoBRvNQEUo2'
let token = new fernet.Token({secret, token: message, ttl:0})
console.log(token.decode())