// calirojas@outlook.com

import Vue from 'vue'
import App from './App.vue'
import Notifications from 'vue-notification'
import VuejsDialog from 'vuejs-dialog'
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.min.css'
import 'vuejs-dialog/dist/vuejs-dialog.min.css'

Vue.use(Notifications)
Vue.use(VuejsDialog)
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
