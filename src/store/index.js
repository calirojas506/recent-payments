import Vue from 'vue'
import Vuex from 'vuex'
const fb = require('@/firebase/')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    paymentsData: []
  },
  getters: {
    getPaymentsData (state) {
      return state.paymentsData
    }
  },
  actions: {
    fetchPayments({ commit }) {
      return new Promise((resolve, reject) => {
        fb.paymentsCollection.get().then(res => {
          let docs = res.docs.map(doc => {
            return Object.assign(doc.data(), {_key: doc.id})
          })
          commit('setPaymentsData', docs)
          resolve()
        }).catch(err => {
          reject(err)
        })
      })
    },
    // eslint-disable-next-line
    deletePayment ({ commit }, documentId) {
      return new Promise((resolve, reject) => {
        fb.paymentsCollection.doc(documentId).delete().then(val => {
          resolve(val)
        }).catch(err => {
          reject(err)
        })
      })
    },
    // eslint-disable-next-line
    updatePayment ({ commit }, payload) {
      return new Promise((resolve, reject) => {
        fb.paymentsCollection.doc(payload.id).update(payload.data).then(val => {
          resolve(val)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }, 
  mutations: {
    setPaymentsData (state, payload) {
      state.paymentsData = payload
    }
  }
})