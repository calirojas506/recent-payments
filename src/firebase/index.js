import firebase from 'firebase/app'
// eslint-disable-next-line
import firestore from 'firebase/firestore'

// Initialize Firebase
const config = {
  apiKey: 'AIzaSyCodwJPqp1akZofnersqnzh8CliQqlaiLI',
  authDomain: 'recent-payments-demo.firebaseapp.com',
  databaseURL: 'https://recent-payments-demo.firebaseio.com',
  projectId: 'recent-payments-demo',
  storageBucket: '',
  messagingSenderId: '116984369431'
}
firebase.initializeApp(config)

const db = firebase.firestore()
const settings = {
    timestampsInSnapshots: true
}
db.settings(settings)

const paymentsCollection = db.collection('payments')

export {
    db,
    paymentsCollection
}